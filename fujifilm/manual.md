# 富士フィルム案件　運用マニュアル

## サーバー情報
//ica_public2/work/fujifilm/サーバー情報

instaxjpが日本語版、instax.comがグローバル版となります。
本番URLはこちら

* [http://instax.jp/（日本語）](http://instax.jp/)
* [http://instax.com/（多言語）](http://instax.com/)

接続に秘密鍵が必要になります。接続方法の詳細はサーバー情報内に記載されていますので、そちらを元に接続してください。
MacでFileZillaの接続が云々とありますが、2020年1月22日現在私の環境では接続ができました。
FileZillaのバージョンにも由来するかもしれませんので、最新のクライアントで試してみてください。
もしうまくいかない場合は、お手数ですがWinSCPから接続をお願いいたします。  

鍵ファイルは共有サーバーに置いてあります。


## 作業の進行について
以前は本番サーバーに直接アップロードしてもよかったのですが、クライアントの方針変更で、本番サーバーへのアップロードは原則クライアントのみで行うことになりました。  
（ダウンロードは行って構いません）  
その為、公開時は先方に差分データを納品し、あちら側で公開を行います。
先方のステージング環境は自由に変更を加えて問題ありませんが、本番環境に直接アップロードを行わないよう注意してください。

また、先方側で本番サーバーのファイルに変更が頻繁に入ります。
作業中に変更が入ることも多い為、ロールバック防止も兼ねローカル環境でgitの運用を推奨します。  
（gitについては別途資料を参考にしてください）

