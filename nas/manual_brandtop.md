# NASブランドサイト　トップページ運用マニュアル

## 修正箇所
※クライアント側で度々更新がされています。作業前に必ず本番サーバーよりダウンロードしてください。  
静的コンテンツのため、特に難しい変更箇所は少ないと思います。

### メインビジュアルの更新
動画の差し替えはjs側に変更が必要になります。
js/vid_vimeo.js内20行目付近

``` javascript
var player = new Vimeo.Player('video', {
  id: 348967832,
  width: '100%',
  muted: true,
  autoplay: true,
  loop: true,
  background: true,
});

```

こちらのIDを変更してください。

動画を静的画像に差し替える場合、また静的画像から動画に戻す場合はindex.htmlの下記の箇所を変更してください。

``` html
<!-- 動画あり -->
<div class="vid vimeo">
  <div id="video"></div>
</div>

<!-- 動画なし -->
<div class="vid vimeo" style="display: none">
  <div id="video"></div>
</div>

```

``` html
<!-- 動画あり、静止画なしの場合はコメントはずす -->
<div class="static">
  <img src="./img_renew/pc_main_20200101.jpg" alt="" class="showpc">
  <img src="./img_renew/sp_main_20200101.jpg" alt="" class="showsp">
</div>
```

``` html
<!-- 動画あり -->
<div class="wrap_mv_text">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 265" id="svg_txt">
    <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./img_renew/mv_txt.svg" width="640" height="265" mask="url(#mask)"></image>
        ...
  </svg>
  <p class="mv_txt_s">
    <img src="./img_renew/mv_txt_s.png" alt="">
  </p>
</div>

<!-- 動画なし -->
<div class="wrap_mv_text" style="display: none">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 265" id="svg_txt">
    <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./img_renew/mv_txt.svg" width="640" height="265" mask="url(#mask)"></image>
        ...
  </svg>
  <p class="mv_txt_s">
    <img src="./img_renew/mv_txt_s.png" alt="">
  </p>
</div>
```

## 店舗一覧の更新
店舗の一覧はjs/location.jsonで管理しています。
