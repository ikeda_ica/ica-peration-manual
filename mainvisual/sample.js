import {TimelineMax, Power0, Power1, Power2} from "gsap";

function animateMVPC() {
  let tl = new TimelineMax({
    repeat: -1
  });

  // scene 1
  tl.to("#mv_pc #scene1 .cc", 1, {
    opacity: 1,
    ease: Power1.easeOut
  });
  tl.to("#mv_pc #scene1 .white", 3, {
    opacity: 0,
    delay: .5,
    ease: Power1.easeOut
  });

  tl.to("#mv_pc #scene1 .cloud_lr", 5.5, {
    opacity: 0,
    x: "20%",
    delay: -2.5,
    ease: Power0.easeNone
  });
  tl.to("#mv_pc #scene1 .cloud_rl", 5.5, {
    opacity: 0,
    x: "-20%",
    delay: -8,
    ease: Power0.easeNone
  });

  tl.to("#mv_pc #scene1 .cc", 1, {
    opacity: 0,
    delay: -2,
    ease: Power1.easeOut
  });
  tl.to("#mv_pc #scene1 .mv_image", 2, {
    opacity: 1,
    ease: Power1.easeOut
  });
  tl.to("#mv_pc #scene1 .title", 2, {
    opacity: 1,
    delay: -3,
    ease: Power1.easeOut
  });

  tl.to("#mv_pc #scene1", 1, {
    opacity: 0,
    ease: Power2.easeOut
  });
  tl.to("#mv_pc #scene2", 1, {
    opacity: 1,
    delay: -1,
    ease: Power2.easeOut
  });
  tl.to("#mv_pc .bg_scene234", 1, {
    opacity: 1,
    delay: -1,
    ease: Power2.easeOut
  });

  tl.to("#mv_pc #scene2 .item_1 .tube", 1, {
    rotation: "0deg",
    ease: Power2.easeInOut,
    delay: -.5
  });  
  tl.to("#mv_pc #scene2 .item_2 .funnel", 1, {
    y: "0%",
    delay: -1,
    ease: Power2.easeInOut
  });  
  tl.to("#mv_pc #scene2 .item", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene2 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeIn
  });
  tl.to("#mv_pc #scene2 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_pc #scene2 .item_1 .item_sub_2", .3, {
    scale: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_pc #scene2 .item_1 .item_sub_1", .3, {
    scale: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_1", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_2", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_3", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_6", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_4", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_5", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene2 .item_2 .item_sub_7", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });

  tl.to("#mv_pc #scene2", 1, {
    x: "-100%",
    delay: .5,
    ease: Power2.easeInOut,
  });  
  tl.to("#mv_pc #scene3", 1, {
    x: "0%",
    delay: -1,
    ease: Power2.easeInOut,
  });  

  tl.to("#mv_pc #scene3 .item_3 .flask", 1, {
    rotation: "0deg",
    ease: Power2.easeInOut,
    delay: -.5
  });
  tl.to("#mv_pc #scene3 .item_4 .cup_main", 1, {
    rotation: "0deg",
    ease: Power2.easeInOut,
    delay: -1
  });

  tl.to("#mv_pc #scene3 .item_1", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene3 .item_2", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.6
  });
  tl.to("#mv_pc #scene3 .item", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -1
  });

  tl.to("#mv_pc #scene3 .catch", .3, {
    scale: .9,
    opacity: 1,
    ease: Power2.easeIn
  });
  tl.to("#mv_pc scene3 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeOut
  });

  tl.to("#mv_pc #scene3 .item_3 .item_sub_2", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });
  tl.to("#mv_pc #scene3 .item_3 .item_sub_3", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene3 .item_3 .item_sub_4", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene3 .item_3 .item_sub_1", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });  

  tl.to("#mv_pc #scene3 .item_4 .item_sub_1", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });
  tl.to("#mv_pc #scene3 .item_4 .item_sub_3", .3, {
    scale: 1,
    ease: Power2.easeOut,
    delay: -.3
  });
  tl.to("#mv_pc #scene3 .item_4 .item_sub_2", .3, {
    scale: 1,
    ease: Power2.easeOut,
  });  

  tl.to("#mv_pc #scene3", 1, {
    x: "-100%",
    delay: .5,
    ease: Power2.easeInOut,
  });  
  tl.to("#mv_pc #scene4 .sweets", 1, {
    x: "0%",
    delay: -1,
    ease: Power2.easeInOut,
  });  

  tl.to("#mv_pc #scene4 .title_l_catch", 1, {
    opacity: 1,
    scale: 1,
    delay: .5,
    ease: Power1.easeOut,
  });  
  tl.to("#mv_pc #scene4 .title_l_main", 1, {
    opacity: 1,
    scale: 1,
    delay: -.5,
    ease: Power1.easeOut,
  });  
  tl.to("#mv_pc #scene4 .white", 1, {
    opacity: 1,
    delay: 3,
    ease: Power2.easeOut
  });
}

function animateMVSP() {
  let tl = new TimelineMax({
    repeat: -1
  });

  // scene 1
  tl.to("#mv_sp #sp_scene1 .cc", 1, {
    opacity: 1,
    ease: Power1.easeOut
  });
  tl.to("#mv_sp #sp_scene1 .white", 3, {
    opacity: 0,
    delay: .5,
    ease: Power1.easeOut
  });

  tl.to("#mv_sp #sp_scene1 .cloud_lr", 5.5, {
    opacity: 0,
    x: "20%",
    delay: -2.5,
    ease: Power0.easeNone
  });
  tl.to("#mv_sp #sp_scene1 .cloud_rl", 5.5, {
    opacity: 0,
    x: "-20%",
    delay: -8,
    ease: Power0.easeNone
  });

  tl.to("#mv_sp #sp_scene1 .cc", 1, {
    opacity: 0,
    delay: -2,
    ease: Power1.easeOut
  });
  tl.to("#mv_sp #sp_scene1 .mv_image", 2, {
    opacity: 1,
    ease: Power1.easeOut
  });
  tl.to("#mv_sp #sp_scene1 .title", 2, {
    opacity: 1,
    delay: -3,
    ease: Power1.easeOut
  });

  tl.to("#mv_sp #sp_scene1", 1, {
    opacity: 0,
    ease: Power2.easeOut
  });
  tl.to("#mv_sp #sp_scene23", 1, {
    opacity: 1,
    delay: -1,
    ease: Power2.easeOut
  });
  tl.to("#mv_sp .bg_scene234", 1, {
    opacity: 1,
    delay: -1,
    ease: Power2.easeOut
  });

  tl.to("#mv_sp #sp_scene23 .item_1 .tube", 1, {
    rotation: "0deg",
    ease: Power2.easeInOut,
    delay: -.5
  });  
  tl.to("#mv_sp #sp_scene23 .item_1 .item", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.3
  });
  tl.to("#mv_sp #sp_scene23 .item_1 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_sp #sp_scene23 .item_1 .item_sub", .3, {
    scale: 1,
    delay: -.3,
    ease: Power2.easeOut
  });

  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 1,
    delay: .5,
    ease: Power2.easeInOut,
  });  
  tl.set("#mv_sp #sp_scene23 .item_1", {
    opacity: 0,
  });  
  tl.set("#mv_sp #sp_scene23 .item_2", {
    opacity: 1,
  });  
  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 0,
    ease: Power2.easeInOut,
  });  

  tl.to("#mv_sp #sp_scene23 .item_2 .funnel", 1, {
    y: "0%",
    delay: -.5,
    ease: Power2.easeInOut
  });  
  tl.to("#mv_sp #sp_scene23 .item_2 .item", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.3
  });
  tl.to("#mv_sp #sp_scene23 .item_2 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_sp #sp_scene23 .item_2 .item_sub", .3, {
    scale: 1,
    delay: -.3,
    ease: Power2.easeOut
  });  

  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 1,
    delay: .5,
    ease: Power2.easeInOut,
  });  
  tl.set("#mv_sp #sp_scene23 .item_2", {
    opacity: 0,
  });  
  tl.set("#mv_sp #sp_scene23 .item_3", {
    opacity: 1,
  });  
  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 0,
    ease: Power2.easeInOut,
  });  

  tl.to("#mv_sp #sp_scene23 .item_3 .flask", 1, {
    rotation: "0deg",
    delay: -.5,
    ease: Power2.easeInOut
  });  
  tl.to("#mv_sp #sp_scene23 .item_3 .item_in_1", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.3
  });
  tl.to("#mv_sp #sp_scene23 .item_3 .item_in_2", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.6
  });
  tl.to("#mv_sp #sp_scene23 .item_3 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_sp #sp_scene23 .item_3 .item_sub", .3, {
    scale: 1,
    delay: -.3,
    ease: Power2.easeOut
  });  
 
  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 1,
    delay: .5,
    ease: Power2.easeInOut,
  });  
  tl.set("#mv_sp #sp_scene23 .item_3", {
    opacity: 0,
  });  
  tl.set("#mv_sp #sp_scene23 .item_4", {
    opacity: 1,
  });  
  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 0,
    ease: Power2.easeInOut,
  });    

  tl.to("#mv_sp #sp_scene23 .item_4 .cup_main", 1, {
    rotation: "0deg",
    ease: Power2.easeInOut,
    delay: -.5
  });  
  tl.to("#mv_sp #sp_scene23 .item_4 .item", 1, {
    scale: 1,
    ease: Power2.easeInOut,
    delay: -.3
  });
  tl.to("#mv_sp #sp_scene23 .item_4 .catch", .3, {
    scale: 1,
    opacity: 1,
    ease: Power2.easeOut
  });
  tl.to("#mv_sp #sp_scene23 .item_4 .item_sub", .3, {
    scale: 1,
    delay: -.3,
    ease: Power2.easeOut,
  });    

  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 1,
    delay: .5,
    ease: Power2.easeInOut,
  });  
  tl.set("#mv_sp #sp_scene23 .item_4", {
    opacity: 0,
  });  
  tl.set("#mv_sp #sp_scene4 .sweets", {
    opacity: 1,
  });  
  tl.set("#mv_sp #sp_scene4 .gr_cloud", {
    opacity: 1,
  });
  tl.to("#mv_sp #sp_scene23 .white", 1, {
    opacity: 0,
    ease: Power2.easeInOut,
  });  
  tl.to("#mv_sp #sp_scene4 .cloud_lr", 3, {
    opacity: 0,
    x: "20%",
    delay: -1,
    ease: Power0.easeNone
  });
  tl.to("#mv_sp #sp_scene4 .cloud_rl", 3, {
    opacity: 0,
    x: "-20%",
    delay: -3,
    ease: Power0.easeNone
  });

  tl.to("#mv_sp #sp_scene4 .title_l_catch", 1, {
    opacity: 1,
    scale: 1,
    delay: .5,
    ease: Power1.easeOut,
  });  
  tl.to("#mv_sp #sp_scene4 .title_l_main", 1, {
    opacity: 1,
    scale: 1,
    delay: -.5,
    ease: Power1.easeOut,
  });  
  tl.to("#mv_sp #sp_scene4 .white", 1, {
    opacity: 1,
    delay: 3,
    ease: Power2.easeOut
  });  
}

window.addEventListener("load", function(){
  animateMVPC();
  animateMVSP();
});