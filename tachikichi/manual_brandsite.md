# たち吉ブランドサイト　運用マニュアル

## サーバー情報
//ica_public2/work/たち吉/01data/FTP情報等/FTP情報cojpとjp.txt

## 修正箇所
サイトの構成上、静的ページとwordpressにて管理されているページが混在しています。
以下ディレクトリおよびその下層はwordpressになります。
* https://www.tachikichi.co.jp/
* https://www.tachikichi.co.jp/news/
* https://www.tachikichi.co.jp/shop/
* https://www.tachikichi.co.jp/category/
* https://www.tachikichi.co.jp/contact-product/
* https://www.tachikichi.co.jp/contact-shop/
* https://www.tachikichi.co.jp/contact-inquiry/
* https://www.tachikichi.co.jp/contact-purchase/
* https://www.tachikichi.co.jp/contact-other/
* https://www.tachikichi.co.jp/contact-en/
* https://www.tachikichi.co.jp/recruit/

上記ディレクトリ配下については、こちらにテーマファイルが格納されています。
* /page/wp-content/themes/tachikichi/ (PC)
* /page/wp-content/themes/wp-mobi/ (スマホ)

静的ページについてはSP版用のHTMLが存在します。こちらも更新が必要になります。(sp/ ディレクトリ以下)
