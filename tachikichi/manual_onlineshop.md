# たち吉オンラインショップ　運用マニュアル

## サーバー情報
//ica_public2/work/たち吉/01data/FTP情報等/FTP情報cojpとjp.txt

## 修正箇所
文字の修正や画像の差し替え等、主に変更が入るディレクトリは以下になります。
* /app/template/tachikichi/
* /html/contents/app/Resources/views/
* /html/user_data/
* /html/shop/

/app/template/tachikichi/以下のディレクトリ構造
* Block/: ECCUBEで使用されているテンプレート（ヘッダー、フッター、サイドバー等）
* Cart/: カート内ページ
* Contact/: お問い合わせフォーム
* Entry/: 会員登録フォーム
* Forgot/: パスワード再発行フォーム
* Form/: フォームテンプレート
* GmoPaymentGateway/: クレジットカード決済に関わる部分です。変更を加えないでください。
* Help/: 使用していません
* Mail/: ECCUBEから送信されるメールのテンプレート
* Mypage/: マイページ
* Product/: 商品一覧、商品詳細ページ
* Shopping/: 発注画面、ログイン画面、注文完了画面

こちらのディレクトリ以下はただファイルに変更を加えただけでは反映されません。
修正作業が終わりましたら、/app/cache/twig/production/tachikichi/ 以下のファイルを全て削除してください。
（削除完了までに時間がかかります）

/html/contents/app/Resources/views/ 以下のディレクトリについても同様に、ファイルの修正があった際は、
/html/contents/var/cache/prod/twig/ 以下のファイルを全て削除してください。

その他の箇所については即時反映されます。

## ヘッダー、サイドバーなど、共通箇所の更新

サイトの構造上、共通箇所については3箇所変更が必要になります。

●ヘッダーの変更箇所
* /app/template/tachikichi/Block/header_new.twig
* /html/contents/app/Resources/views/header.html.twig
* /html/shop/template/header.html

●サイドバーの変更箇所
* /app/template/tachikichi/Block/sidebar_new.twig
* /html/contents/app/Resources/views/lefter.html.twig
* /html/shop/template/sidebar.html

## 新規テンプレート作成方法
EC-CUBE上の複数ページにコンテンツを差し込みたい時があると思います。
下記の手順にて追加が可能です。

1. 管理画面上、コンテンツ管理→ブロック管理をクリック
2. ページ最下部の新規入力をクリック
3. ブロック名、ファイル名を入力いただき、ブロックデータ内にソースコードを入力
4. 登録後、ページ管理より先ほど作成したテンプレートを差し込みたいページの右端の[…]をクリック
5. レイアウト編集をクリック
6. 下記画像のような画面が出てきますので、右から新しく作成した項目をドラッグ＆ドロップで挿入したい箇所に持ってきてください。  
![レイアウト管理](./screenshot_block.png)
7. 作業完了後、登録をクリックすると登録されます。

## yasuyo更新
ディレクトリは以下
/html/shop/yasuyo

新しいレシピが届きましたら更新をお願いいたします。  
レシピ一覧の更新ですが、サイトの構造上更新方法が特殊になっています。
以下箇所が一覧部分のソースです。
```html
<!-- PC版 -->
<div class="wrap-list-recipe">
    <ul class="list-recipe list-left">
        ...
    </ul>
    <ul class="list-recipe list-center">
        ...
    </ul>
    <ul class="list-recipe list-right">
        ...
    </ul>
</div>

<!-- SP版 -->
<div class="wrap-list-recipe-sp">
    <ul class="list-recipe">
        ...
    </ul>
</div>
```

PC版ですが、まずリストタグをコピーし、現在掲載されているレシピの情報を入力します。そのレシピは`list-recipe list-right`配下のdivに入れてください。  
その後、`list-recipe list-right`配下のdiv全てを選択し、`list-recipe list-left`より上にペーストします。  
最後にクラス名を以下の通りに変更すれば完了です。  
```html
<!-- 修正前 -->
<div class="wrap-list-recipe">
    <ul class="list-recipe list-right">
        ...
    </ul>
    <ul class="list-recipe list-left">
        ...
    </ul>
    <ul class="list-recipe list-center">
        ...
    </ul>
</div>

<!-- 修正後 -->
<div class="wrap-list-recipe">
    <ul class="list-recipe list-left">
        ...
    </ul>
    <ul class="list-recipe list-center">
        ...
    </ul>
    <ul class="list-recipe list-right">
        ...
    </ul>
</div>
```

SP版はそのまま上に追加していけば問題ありません。後は一番上の最新のレシピ情報を入れれば終わりです。
